<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Section;
use Illuminate\Support\Facades\Auth;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the input of the instructor
        $this->validate($request, [
            'title' => ['required', 'unique:courses', 'max:255'],
            'language' => 'required',
            'game' => 'required',
        ]);

        // Create Course
        $course = new Course;
        $course->title = $request->input('title');
        $course->description = '';
        $course->user_id = Auth::id();
        $course->language = $request->input('language');
        $course->game = $request->input('game');
        $course->status = 'inprogress';
        $course->price = 0;
        $course->sale_price = 0;
        $course->save();

        // create the first and undeletable section "introduction" for the course
        $section = new Section;
        $section->name = "Introduction";
        $section->course_id = $course->id;
        $section->position = 1;
        $section->save();


        return redirect('/instructor/edit/' . $course->id)->with('success', 'You succesfully created a new Course!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the input of the instructor
        $this->validate($request, [
            'title' => ['required', 'unique:courses', 'max:255'],
            'language' => 'required',
            'game' => 'required',
        ]);

        // Create Course
        $course = Course::find($id);
        $course->title = $request->input('title');
        $course->language = $request->input('language');
        $course->game = $request->input('game');
        $course->save();

        return redirect('/instructor/edit/' . $course->id)->with('success', 'Course Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect('/instructor/mycourses/')->with('success', 'Course Removed');
    }
}
